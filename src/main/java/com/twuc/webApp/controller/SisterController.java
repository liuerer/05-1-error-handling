package com.twuc.webApp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SisterController {

    @GetMapping("/api/sister-errors/illegal-argument")
    public ResponseEntity<String> throw_illegalArgumentException_sister() {
        throw new IllegalArgumentException();
    }
}
