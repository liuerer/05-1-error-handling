package com.twuc.webApp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.security.AccessControlException;

@RestController
public class ExceptionController {

    @GetMapping("/api/errors/default")
    public ResponseEntity<String> get_exception(){
        throw new RuntimeException();
    }

    @GetMapping("/api/errors/illegal-argument")
    public ResponseEntity<String> throw_IllegalArgumentException() { throw new IllegalArgumentException(); }

    @ExceptionHandler
    public ResponseEntity<String> handler_illegalArgumentException(IllegalArgumentException exception){
        return ResponseEntity.status(500)
                .body("Something wrong with the argument");
    }

    @GetMapping("/api/errors/null-pointer")
    public ResponseEntity<String> get_null_pointer_exception(){
        throw new NullPointerException();
    }

    @GetMapping("/api/errors/arithmetic")
    public ResponseEntity<String> get_arithmetic_exception(){
        throw new ArithmeticException();
    }

    @ExceptionHandler({NullPointerException.class, ArithmeticException.class})
    public ResponseEntity<String> null_point_and_arithmeticException(){
        return ResponseEntity.status(418)
                .body("Something wrong with the argument");
    }

}
