package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class ExceptionControllerTest {

    @Autowired
    TestRestTemplate template;

    @Test
    void should_get_status_runTime_exception() {
        ResponseEntity<String> entity = template.getForEntity("/api/errors/default", String.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
    }

    @Test
    void should_get_status_internal_server_error() {
        ResponseEntity<String> entity = template.getForEntity("/api/errors/illegal-argument", String.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
        assertEquals("Something wrong with the argument", entity.getBody());
    }

    @Test
    void should_get_status_418_when_throw_NullPointerException() {
        ResponseEntity<String> entity = template.getForEntity("/api/errors/null-pointer", String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        assertEquals("Something wrong with the argument", entity.getBody());
    }

    @Test
    void should_get_status_418_when_throw_ArithmeticException() {
        ResponseEntity<String> entity = template.getForEntity("/api/errors/arithmetic", String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        assertEquals("Something wrong with the argument", entity.getBody());
    }
}
