package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class BrotherControllerTest {

    @Autowired
    TestRestTemplate template;

    @Test
    void should_get_status_418_brother() {
        ResponseEntity<String> entity = template.getForEntity("/api/brother-errors/illegal-argument", String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        assertEquals("{\"message\":\"Something wrong with brother or sister.\"}", entity.getBody());
    }
}
